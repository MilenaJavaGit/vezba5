var wafepa = angular.module("WafepaApp", ['ngRoute']);

wafepa.service('standoviService', function($http){

	this.baseUrl = "/api/standovi";
	
	this.getStandovi = function(config){
		return $http.get(this.baseUrl, config);
	}

});


wafepa.controller("activitiesCtrl", function($scope, $http, $location){

	var baseUrl = "/api/activities";
	
	$scope.activities = [];
	
	$scope.newActivity = {};
	$scope.newActivity.name = "";
	
	$scope.gotActivities = false;
	
	var getActivities = function(){
		
		var promise = $http.get(baseUrl);
		promise.then(
			function success(data){
				$scope.activities = data.data;
				$scope.gotActivities = true;
			},
			function greska(data){
				console.log(data);
			}
		);		
	}
	
	getActivities();
	
	$scope.addActivity = function(){
		
		var promise = $http.post(baseUrl, $scope.newActivity);
		promise.then(
			function success(data){
				getActivities();
				$scope.newActivity.name = "";
			},
			function error(data){
				alert("Something went wrong!");
			}
		);
	}
	
	$scope.editActivity = function(aid){
		$location.path("/activities/edit/" + aid);
		//alert(aid);
	}
	
	$scope.deleteActivity = function(id){
		
		var promise = $http.delete(baseUrl + "/" + id);
		promise.then(
			function success(data){
				getActivities();
			},
			function error(data){
				alert("Neuspesno brisanje.")
			}
		);
	}

});

wafepa.controller("editActivityCtrl", function($scope, $http, $routeParams, $location){
	
	var baseUrl = "/api/activities/";
	var id = $routeParams.aid;
	
	$scope.oldActivity = {};
	$scope.oldActivity.name = "";
	
	var getActivity = function(){
		$http.get(baseUrl + id).
			then(
				function success(data){
					$scope.oldActivity = data.data;
				},
				function error(data){
					
				}
			);	
	}
	
	getActivity();
	
	$scope.edit = function(){
		$http.put(baseUrl + id, $scope.oldActivity).
			then(
				function uspeh(data){
					$location.path("/activities");
				},
				function error(data){
					alert("Something went wrong!");
				}
			);
	}
	
});


wafepa.controller("standoviCtrl", function($scope, $http, standoviService){
	
	var baseUrl = "/api/standovi";
	var baseUrlSajmovi = "/api/sajmovi";
	
	$scope.standovi = [];
	$scope.sajmovi = [];
	
	
	$scope.newStand = {};
	$scope.newStand.zakupac = "";
	$scope.newStand.povrsina = "";
	$scope.newStand.sajamId = "";
	
	
	$scope.searchedStand = {};
	$scope.searchedStand.zakupac = "";
	$scope.searchedStand.minP = "";
	$scope.searchedStand.maxP = "";
	
	$scope.pageNum = 0;
	$scope.totalPages = 0;
	
	var getStandovi = function(){
	
		var config = {params:{}};
		
		if($scope.searchedStand.zakupac != ""){
			config.params.zakupac = $scope.searchedStand.zakupac;
		}
		if($scope.searchedStand.minP != ""){
			config.params.minP = $scope.searchedStand.minP;
		}
		if($scope.searchedStand.maxP != ""){
			config.params.maxP = $scope.searchedStand.maxP;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = standoviService.getStandovi(config);
		promise.then(
			function success(data){
				$scope.standovi = data.data;
				
				$scope.totalPages = data.headers("totalPages");
			},
			function error(data){
				console.log(data);
				alert("Neuspesno dobavljanje standova.")
			}
		);
	}
	
	getStandovi();
	
	var getSajmovi = function(){
		
		$http.get(baseUrlSajmovi).then(
			function success(data){
				$scope.sajmovi = data.data;
			},
			function error(data){
				alert("Neuspesno dobavljanje sajmova.")
			}	
		);
	}
	
	getSajmovi();
	
	$scope.addStand = function(){
		
		$http.post(baseUrl, $scope.newStand).then(
			function success(data){
				getStandovi();
			},
			function error(data){
				alert("Nije uspelo dodavanje standa.");
			}
		);		
	}
	
	$scope.search = function(){
		//console.log($scope.searchedStand);
		getStandovi();
	}
	
	$scope.go = function(par){
		$scope.pageNum = $scope.pageNum + par;
		getStandovi();
	}
	
});

wafepa.controller("knjigeCtrl", function($scope, $http, $location){
	var baseUrl = "/api/knjige";
	var baseUrlIzdavaci = "/api/izdavaci";
	
	$scope.knjige =[];
	$scope.izdavaci=[];
	
	$scope.vidljivost = true;
	
	$scope.newKnjiga={};
	$scope.newKnjiga.naziv="";
	$scope.newKnjiga.pisac="";
	$scope.newKnjiga.ISBN="";
	$scope.newKnjiga.kolicina="";
	$scope.newKnjiga.cena="";
	$scope.newKnjiga.izdavacId="";
	$scope.newKnjiga.izdavacNaziv="";
	
	$scope.searchedKnjiga={};
	$scope.searchedKnjiga.naziv="";
	$scope.searchedKnjiga.pisac="";
	$scope.searchedKnjiga.maxK="";
	
	$scope.pageNum=0;
	$scope.totalPages=0;
	
	var getKnjige = function(){
		var config = {params:{}};
		
		if($scope.searchedKnjiga.naziv !=""){
			config.params.naziv = $scope.searchedKnjiga.naziv;
		}
		if($scope.searchedKnjiga.pisac !=""){
			config.params.pisac = $scope.searchedKnjiga.pisac;
		}
		if($scope.searchedKnjiga.maxK !=""){
			config.params.maxK = $scope.searchedKnjiga.maxK;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		$http.get(baseUrl, config).then(
			function success(data){
				$scope.knjige = data.data;
				$scope.totalPages = data.headers("totalPages");
			},
			function error(data){
				alert("Neuspesno dobavljanje knjiga.")
			}
		);
	}
	getKnjige();
	
	var getIzdavaci = function(){
		$http.get(baseUrlIzdavaci).then(
			function success(data){
				$scope.izdavaci = data.data;
			},
			function error(data){
				alert("Neuspesno dobavljanje izdavaca.")
			}
		);
	}
	getIzdavaci();
	
	$scope.addKnjiga = function(){
		$http.post(baseUrl, $scope.newKnjiga).then(
				function success(data){
					getKnjige();
					$scope.newKnjiga.naziv="";
					$scope.newKnjiga.pisac="";
					$scope.newKnjiga.ISBN="";
					$scope.newKnjiga.kolicina="";
					$scope.newKnjiga.cena="";
					$scope.newKnjiga.izdavacId="";
					$scope.newKnjiga.izdavacNaziv="";
				},
				function error(data){
					alert("Nije uspelo dodavanje knjige.")
				}
		);
	}
	$scope.editKnjige = function(kid){
		$location.path("/knjige/edit/" + kid);
	}
	
	$scope.deleteKnjiga = function(id){
		$http.delete(baseUrl + "/" + id).then(
			function success(data){
				getKnjige();
			},
			function error(data){
				alert("Neuspesno brisanje.")
			}
		);
	}
	
	$scope.search = function(){
		getKnjige();
	}
	$scope.go = function(broj){
		$scope.pageNum = $scope.pageNum + broj;
		getZadaci();
	}
	$scope.kupi = function(id){
		$http.post(baseUrl + "/" + id + "/kupovina").then(
			function success(data){
				getKnjige();
			},
			function error(data){
				alert("Neuspesna kupovina.")
			}
		);
	}
	
});

wafepa.controller("zadaciCtrl", function($scope, $http, $location){
	var baseUrl = "/api/zadaci";
	var baseUrlZaposleni = "/api/zaposleni";
	var baseUrlTipovi = "/api/tipovi";
	
	$scope.zadaci=[];
	$scope.zaposleni=[];
	$scope.tipovi=[];
	
	$scope.vidljivost = true;
	
	$scope.newZadatak={};
	$scope.newZadatak.naziv="";
	$scope.newZadatak.opis="";
	$scope.newZadatak.prioritet="";
	$scope.newZadatak.rok="";
	$scope.newZadatak.zaposleniId="";
	$scope.newZadatak.zaposleniIme="";
	$scope.newZadatak.zaposleniPrezime="";
	$scope.newZadatak.tipId="";
	
	$scope.searchedZadatak={};
	$scope.searchedZadatak.naziv="";
	$scope.searchedZadatak.minP="";
	$scope.searchedZadatak.maxP="";
	$scope.searchedZadatak.zaposleniId="";
	
	$scope.pageNum=0;
	$scope.totalPages=0;
	
	var getZadaci = function(){
		var config = {params:{}};
		
		if($scope.searchedZadatak.naziv !=""){
			config.params.naziv = $scope.searchedZadatak.naziv;
		}
		if($scope.searchedZadatak.minP !=""){
			config.params.minP = $scope.searchedZadatak.minP;
		}
		if($scope.searchedZadatak.maxP !=""){
			config.params.maxP = $scope.searchedZadatak.maxP;
		}
		if($scope.searchedZadatak.zaposleniId !=""){
			config.params.zaposleniId = $scope.searchedZadatak.zaposleniId;
		}
		config.params.pageNum = $scope.pageNum;
		
		$http.get(baseUrl, config).then(
				function success(data){
			$scope.zadaci = data.data;	
			$scope.totalPages = data.headers("totalPages");
			},
			function error(data){
				alert("Neuspesno dobavljanje zadataka.")
			}
		);
	}
	getZadaci();
	
	var getTipovi = function(){
		$http.get(baseUrlTipovi).then(
			function success(data){
				$scope.tipovi = data.data;
			},
			function error(data){
				alert("Neuspesno dobavljanje tipova zadataka.")
			}
		);
	}
	getTipovi();
	
	var getZaposleni = function(){
		$http.get(baseUrlZaposleni).then(
			function success(data){
				$scope.zaposleni = data.data;
			},
			function error(data){
				alert("Neuspesno dobavljanje zaposlenih.")
			}
		);
	}
	getZaposleni();
	
	$scope.addZadatak = function(){
		$http.post(baseUrl, $scope.newZadatak).then(
			function success(data){
				getZadaci();
				$scope.newZadatak.naziv="";
				$scope.newZadatak.opis="";
				$scope.newZadatak.prioritet="";
				$scope.newZadatak.rok="";
				$scope.newZadatak.zaposleniId="";
				$scope.newZadatak.tipId="";
			},
			function error(data){
				alert("Neuspesno dodavanje zadatka.")
			}
		);
	}
	$scope.editZadatka = function(zid){
		$location.path("/zadaci/edit/" + zid);
	}
	
	$scope.deleteZadatak = function(id){
		$http.delete(baseUrl + "/" + id).then(
			function success(data){
				getZadaci();
			},
			function error(data){
				alert("Neuspesno brisanje.")
			}
		);
	}
	$scope.search = function(){
		getZadaci();
	}
	$scope.go = function(broj){
		$scope.pageNum = $scope.pageNum + broj;
		getKnjige();
	}
	$scope.najprioritetniji = function(){
		$scope.searchedZadatak.minP =5;
		$scope.searchedZadatak.maxP =5;
		getZadaci();
	}
	$scope.editNaStranici = function(zadatak){
		$scope.newZadatak = zadatak;
	}
	
	
	
	
});

wafepa.controller("editZadatkaCtrl", function($scope, $http, $routeParams, $location){
	var baseUrl = "/api/zadaci/";
	var id =$routeParams.zid;
	
	$scope.oldZadatak={};
	$scope.oldZadatak.naziv="";
	$scope.oldZadatak.opis="";
	$scope.oldZadatak.prioritet;
	$scope.oldZadatak.rok="";
	
	var getZadatak = function(){
		$http.get(baseUrl + id).then(
			function success(data){
				$scope.oldZadatak = data.data;
			},
			function error(data){
				alert("Izmena nije uspela.")
			}
		);
	}
	getZadatak();
	
	$scope.editZadatak = function(){
		$http.put(baseUrl +id, $scope.oldZadatak).then(
			function success(data){
				$location.path("/zadaci");
			},
			function error(data){
				alert("Izmena zadatka nije uspela.")
			}
		);
	}
});

wafepa.controller("editKnjigeCtrl", function($scope, $http, $routeParams, $location){
	var baseUrl = "/api/knjige/";
	var id = $routeParams.kid;
	
	$scope.oldKnjiga = {};
	$scope.oldKnjiga.naziv="";
	$scope.oldKnjiga.pisac="";
	$scope.oldKnjiga.ISBN="";
	$scope.oldKnjiga.kolicina="";
	$scope.oldKnjiga.cena="";
	
	var getKnjiga = function(){
		$http.get(baseUrl + id).then(
			function success(data){
				$scope.oldKnjiga = data.data;
			},
			function error(data){
				alert("izmena nije uspela.")
			}
		);
	}
	getKnjiga();
	
	$scope.editKnjiga = function(){
		$http.put(baseUrl + id, $scope.oldKnjiga).then(
			function success(data){
				$location.path("/knjige");
			},
			function error(data){
				alert("Izmena zadatka nije uspela.")
			}
		);
	}
});



wafepa.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/html/partial/home.html'
		})
		.when('/activities', {
			templateUrl : '/app/html/partial/activities.html'
		})
		.when('/activities/edit/:aid', {
			templateUrl : '/app/html/partial/edit_activity.html'
		})
		.when('/standovi', {
			templateUrl : '/app/html/partial/standovi.html'
		})
		.when('/knjige', {
			templateUrl : '/app/html/partial/knjige.html'
		})
		.when('/knjige/edit/:kid', {
			templateUrl : '/app/html/partial/edit_knjige.html'
		})
		.when('/zadaci', {
			templateUrl : '/app/html/partial/zadaci.html'
		})
		.when('/zadaci/edit/:zid', {
			templateUrl : '/app/html/partial/edit_zadatka.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);
