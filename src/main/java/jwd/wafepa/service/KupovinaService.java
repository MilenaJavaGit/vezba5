package jwd.wafepa.service;

import jwd.wafepa.model.Kupovina;

public interface KupovinaService {
	Kupovina buyABook(Long id);

}
