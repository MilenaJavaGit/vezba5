package jwd.wafepa.service;

import java.util.List;

import jwd.wafepa.model.Zaposleni;

public interface ZaposleniService {
	
	List<Zaposleni> findAll();
	Zaposleni findOne(Long id);
	void save(Zaposleni zaposleni);
	void remove(Long id);

}
