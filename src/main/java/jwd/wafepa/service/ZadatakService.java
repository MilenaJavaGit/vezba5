package jwd.wafepa.service;

import org.springframework.data.domain.Page;

import org.springframework.data.repository.query.Param;

import jwd.wafepa.model.Zadatak;

public interface ZadatakService {
	Page<Zadatak> findAll(int pageNum);
	Zadatak findOne(Long id);
	void save(Zadatak zadatak);
	void remove(Long id);
	Page<Zadatak> findByZaposleniId(int pageNum, Long zaposleniId);
	Page<Zadatak> pretraga(
			@Param("naziv") String naziv, 
			@Param("minP") Integer min, 
			@Param("maxP") Integer max,
			@Param("zaposleniId") Long zaposleniId,
			int page);
	
	

}
