package jwd.wafepa.service;

import java.util.List;

import jwd.wafepa.model.TipZadatka;

public interface TipZadatkaService {
	
	List<TipZadatka> findAll();
	TipZadatka findOne(Long id);
	void save(TipZadatka tipZadatka);
	void remove(Long id);

}
