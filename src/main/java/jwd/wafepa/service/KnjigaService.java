package jwd.wafepa.service;

import org.springframework.data.domain.Page;

import org.springframework.data.repository.query.Param;

import jwd.wafepa.model.Knjiga;

public interface KnjigaService {
	Page<Knjiga> findAll(int pageNum);
	Knjiga findOne(Long id);
	void save(Knjiga knjiga);
	void remove(Long id);
	Page<Knjiga> findByIzdavacId(int pageNum, Long izdavacId);
	Page<Knjiga> pretraga(
			@Param("naziv") String naziv, 
			@Param("pisac") String pisac, 
			@Param("maxK") Integer max,
		
			int page);

}
