package jwd.wafepa.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.wafepa.model.Zadatak;
import jwd.wafepa.repository.ZadatakRepository;
import jwd.wafepa.service.ZadatakService;

@Service
@Transactional
public class JpaZadatakServiceImpl implements ZadatakService{
	@Autowired
	private ZadatakRepository zadatakRepository;

	@Override
	public Page<Zadatak> findAll(int pageNum) {
		
		return zadatakRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public Zadatak findOne(Long id) {
		
		return zadatakRepository.findOne(id);
	}

	@Override
	public void save(Zadatak zadatak) {
		zadatakRepository.save(zadatak);
		
	}

	@Override
	public void remove(Long id) {
		zadatakRepository.delete(id);
		
	}

	@Override
	public Page<Zadatak> findByZaposleniId(int pageNum, Long zaposleniId) {
	
		return zadatakRepository.findByZaposleniId(zaposleniId, new PageRequest(pageNum, 5));
	}

	@Override
	public Page<Zadatak> pretraga(String naziv, Integer min, Integer max, Long zaposleniId, int page) {
		if(naziv!=null){
			naziv = "%" + naziv + "%";
		}
		return zadatakRepository.pretraga(naziv, min, max, zaposleniId, new PageRequest(page, 5));
	}
	
	

}
