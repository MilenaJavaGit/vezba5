package jwd.wafepa.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.wafepa.model.Knjiga;
import jwd.wafepa.model.Kupovina;
import jwd.wafepa.repository.KnjigaRepository;
import jwd.wafepa.repository.KupovinaRepository;
import jwd.wafepa.service.KupovinaService;

@Service
@Transactional
public class JpaKupovinaServiceImpl implements KupovinaService{
	@Autowired
	private KupovinaRepository kupovinaRepository;
	@Autowired
	private KnjigaRepository knjigaRepository;
	
	
	@Override
	public Kupovina buyABook(Long id) {
		if(id==null){
			throw new IllegalArgumentException("The id can not be null.");
		}
		
		Knjiga knjiga = knjigaRepository.findOne(id);
		if(knjiga==null){
			throw new IllegalArgumentException("The book with given id do not exit.");
		}
		
		if(knjiga.getKolicina()>0){
			Kupovina kupovina = new Kupovina();
			kupovina.setKnjiga(knjiga);
			knjiga.setKolicina(knjiga.getKolicina() - 1);
			kupovinaRepository.save(kupovina);
			knjigaRepository.save(knjiga);
			return kupovina;
		}
		return null;
	}
	
	

}
