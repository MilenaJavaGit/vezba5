package jwd.wafepa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.wafepa.model.Zaposleni;
import jwd.wafepa.repository.ZaposleniRepository;
import jwd.wafepa.service.ZaposleniService;

@Service
@Transactional
public class JpaZaposleniServiceImpl implements ZaposleniService{
	@Autowired
	private ZaposleniRepository zaposleniRepository;

	@Override
	public List<Zaposleni> findAll() {
		
		return zaposleniRepository.findAll();
	}

	@Override
	public Zaposleni findOne(Long id) {
	
		return zaposleniRepository.findOne(id);
	}

	@Override
	public void save(Zaposleni zaposleni) {
		zaposleniRepository.save(zaposleni);
		
	}

	@Override
	public void remove(Long id) {
		zaposleniRepository.delete(id);
		
	}
	
	
	
	

}
