package jwd.wafepa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.wafepa.model.TipZadatka;
import jwd.wafepa.repository.TipZadatkaRepository;
import jwd.wafepa.service.TipZadatkaService;

@Service
@Transactional
public class JpaTipZadatkaService implements TipZadatkaService{
	@Autowired
	private TipZadatkaRepository tipZadatkaRepository;

	@Override
	public List<TipZadatka> findAll() {
		
		return tipZadatkaRepository.findAll();
	}

	@Override
	public TipZadatka findOne(Long id) {
	
		return tipZadatkaRepository.findOne(id);
	}

	@Override
	public void save(TipZadatka tipZadatka) {
		tipZadatkaRepository.save(tipZadatka);
		
	}

	@Override
	public void remove(Long id) {
		tipZadatkaRepository.delete(id);
		
	}
}
