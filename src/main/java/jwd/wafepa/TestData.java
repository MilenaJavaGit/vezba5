package jwd.wafepa;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jwd.wafepa.model.Activity;
import jwd.wafepa.model.Izdavac;
import jwd.wafepa.model.Knjiga;
import jwd.wafepa.model.Sajam;
import jwd.wafepa.model.Stand;
import jwd.wafepa.model.TipZadatka;
import jwd.wafepa.model.Zadatak;
import jwd.wafepa.model.Zaposleni;
import jwd.wafepa.service.ActivityService;
import jwd.wafepa.service.IzdavacService;
import jwd.wafepa.service.KnjigaService;
import jwd.wafepa.service.SajamService;
import jwd.wafepa.service.StandService;
import jwd.wafepa.service.TipZadatkaService;
import jwd.wafepa.service.ZadatakService;
import jwd.wafepa.service.ZaposleniService;

@Component
public class TestData {

	/*@Autowired
	private UserService userService;
	
	@Autowired
	private AddressService addressService;*/
	
	@Autowired
	private ActivityService activityService;

	@Autowired
	private SajamService sajamService;
	
	@Autowired
	private StandService standService;
	@Autowired
	private KnjigaService knjigaService;
	@Autowired
	private IzdavacService izdavacService;
	@Autowired
	private ZaposleniService zaposleniService;
	@Autowired
	private ZadatakService zadatakService;
	@Autowired
	private TipZadatkaService tipZadatkaService;
	
	@PostConstruct
	public void init(){/*
	       for (int i = 1; i <= 100; i++) {
	            User user = new User();
	            user.setFirstName("First name " + i);
	            user.setLastName("Last name " + i);
	            user.setEmail("email" + i + "@example.com");
	            user.setPassword("123");
	            userService.save(user);

	            for (int j = 1; j <= 3; j++) {
	                Address address = new Address();
	                address.setNumber(j + "c/7");
	                address.setStreat("Narodnog fronta");

	                address.setUser(user);
	                user.addAddress(address);
	                addressService.save(address);
	            }
	       }*/
		for (int j = 0; j <= 3; j++) {
            Activity activity = new Activity();            
            activity.setName("activity" + j);
            activityService.save(activity);
        }
		
		Sajam s1 = new Sajam();
		s1.setCena(123);
		s1.setMesto("Subotica");
		s1.setNaziv("Sajam obrazovanja");
		s1.setOtvaranje("21.12.2017.");
		s1.setZatvaranje("23.12.2017");
		sajamService.save(s1);
		
		Stand st1 = new Stand();
		st1.setPovrsina(123);
		st1.setZakupac("Mitar");
		st1.setSajam(s1);
		standService.save(st1);
		
		Stand st2 = new Stand();
		st2.setPovrsina(122);
		st2.setZakupac("Miric");
		st2.setSajam(s1);
		standService.save(st2);
		
		Stand st3 = new Stand();
		st3.setPovrsina(11);
		st3.setZakupac("Pera");
		st3.setSajam(s1);
		standService.save(st3);
		
		Stand st4 = new Stand();
		st4.setPovrsina(11);
		st4.setZakupac("Pera");
		st4.setSajam(s1);
		standService.save(st4);
		
		Izdavac i1 = new Izdavac();
		i1.setNaziv("Delfi");
		i1.setEmail("delfi@gmail.com");
		i1.setTelefon("011235654");
		izdavacService.save(i1);
		
		Izdavac i2 = new Izdavac();
		i2.setNaziv("Vulkan");
		i2.setEmail("vulkan@gmail.com");
		i2.setTelefon("011258654");
		izdavacService.save(i2);
		
		Knjiga k1 = new Knjiga();
		k1.setNaziv("Faust");
		k1.setPisac("Johan Volfgang Gete");
		k1.setCena(500.00);
		k1.setKolicina(20);
		k1.setIzdavac(i1);
		k1.setISBN("2463445");
		knjigaService.save(k1);
		
		Knjiga k2 = new Knjiga();
		k2.setNaziv("Tako je govorio Zaratustra");
		k2.setPisac("Fridrih Niče");
		k2.setCena(500.00);
		k2.setKolicina(10);
		k2.setIzdavac(i1);
		k2.setISBN("2462545");
		knjigaService.save(k2);
		
		Knjiga k3 = new Knjiga();
		k3.setNaziv("Zločin i Kazna");
		k3.setPisac("Fjodor Mihajlovič Dostojevski");
		k3.setCena(1000.00);
		k3.setKolicina(20);
		k3.setIzdavac(i1);
		k3.setISBN("24354745");
		knjigaService.save(k3);
		
		Knjiga k4 = new Knjiga();
		k4.setNaziv("Slika Dorijana Greja");
		k4.setPisac("Oskar Vajld");
		k4.setCena(499.00);
		k4.setKolicina(10);
		k4.setIzdavac(i2);
		k4.setISBN("244558745");
		knjigaService.save(k4);
		
		Zaposleni z1 = new Zaposleni();
		z1.setIme("Nikola");
		z1.setPrezime("Nikolic");
		z1.setPozicija("administrator");
		z1.setBrojZadataka(5);
		zaposleniService.save(z1);
		
		Zaposleni z2 = new Zaposleni();
		z2.setIme("Marko");
		z2.setPrezime("Markovic");
		z2.setPozicija("administrator");
		z2.setBrojZadataka(10);
		zaposleniService.save(z2);
		
		Zaposleni z3 = new Zaposleni();
		z3.setIme("Mila");
		z3.setPrezime("Milic");
		z3.setPozicija("programer");
		z3.setBrojZadataka(8);
		zaposleniService.save(z3);
		
		TipZadatka t1 = new TipZadatka();
		t1.setNaziv("Modelovanje");
		tipZadatkaService.save(t1);
		
		TipZadatka t2 = new TipZadatka();
		t2.setNaziv("Kodiranje");
		tipZadatkaService.save(t2);
		
		Zadatak zad1 = new Zadatak();
		zad1.setNaziv("Izmodelovati bazu podataka");
		zad1.setOpis("Kontaktirati klijenta");
		zad1.setPrioritet(5);
		zad1.setRok("5.06.2017.");
		zad1.setZaposleni(z1);
		zad1.setTipZadatka(t1);
		zadatakService.save(zad1);
		
		Zadatak zad2 = new Zadatak();
		zad2.setNaziv("Prouciti ogranicenja");
		zad2.setOpis("Izuciti domen");
		zad2.setPrioritet(4);
		zad2.setRok("15.06.2017.");
		zad2.setZaposleni(z2);
		zad2.setTipZadatka(t1);
		zadatakService.save(zad2);
		
		Zadatak zad3 = new Zadatak();
		zad3.setNaziv("Uspostaviti front-end");
		zad3.setOpis("Konsultovati dizajnere");
		zad3.setPrioritet(2);
		zad3.setRok("15.07.2017.");
		zad3.setZaposleni(z3);
		zad3.setTipZadatka(t2);
		zadatakService.save(zad3);
		
		Zadatak zad4 = new Zadatak();
		zad4.setNaziv("Uspostaviti back-end");
		zad4.setOpis("Koristiti Spring Boot");
		zad4.setPrioritet(3);
		zad4.setRok("30.06.2017.");
		zad4.setZaposleni(z2);
		zad4.setTipZadatka(t2);
		zadatakService.save(zad4);
		
	}
}
