package jwd.wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.wafepa.model.Zadatak;
import jwd.wafepa.web.dto.ZadatakDTO;

@Component
public class ZadatakToZadatakDTO implements Converter<Zadatak, ZadatakDTO>{

	@Override
	public ZadatakDTO convert(Zadatak zadatak) {
		ZadatakDTO dto = new ZadatakDTO();
		dto.setId(zadatak.getId());
		dto.setNaziv(zadatak.getNaziv());
		dto.setOpis(zadatak.getOpis());
		dto.setPrioritet(zadatak.getPrioritet());
		dto.setRok(zadatak.getRok());
		dto.setZaposleniId(zadatak.getZaposleni().getId());
		dto.setZaposleniIme(zadatak.getZaposleni().getIme());
		dto.setZaposleniPrezime(zadatak.getZaposleni().getPrezime());
		dto.setTipId(zadatak.getTipZadatka().getId());
		dto.setTipNaziv(zadatak.getTipZadatka().getNaziv());
		return dto;
	}
    
	public List<ZadatakDTO> convert(List<Zadatak> zadaci){
		List<ZadatakDTO> ret = new ArrayList<>();
		
		for(Zadatak z: zadaci){
			ret.add(convert(z));
		}
		return ret;
	}
	
	
}
