package jwd.wafepa.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.wafepa.model.Zadatak;
import jwd.wafepa.service.TipZadatkaService;
import jwd.wafepa.service.ZadatakService;
import jwd.wafepa.service.ZaposleniService;
import jwd.wafepa.web.dto.ZadatakDTO;

@Component
public class ZadatakDTOToZadatak 
             implements Converter<ZadatakDTO, Zadatak>{
	@Autowired
	private ZadatakService zadatakService;
	@Autowired
	private ZaposleniService zaposleniService;
	@Autowired
	private TipZadatkaService tipZadatkaService;
	
	
	@Override
	public Zadatak convert(ZadatakDTO dto) {
		Zadatak zadatak;
		if(dto.getId()==null){
			zadatak = new Zadatak();
			zadatak.setZaposleni(zaposleniService.findOne(
					dto.getZaposleniId()));
			zadatak.setTipZadatka(tipZadatkaService.findOne(
					dto.getTipId()));
			
		}else{
			zadatak = zadatakService.findOne(dto.getId());
		}
		zadatak.setNaziv(dto.getNaziv());
		zadatak.setOpis(dto.getOpis());
		zadatak.setPrioritet(dto.getPrioritet());
		zadatak.setRok(dto.getRok());
		return zadatak;
	}
	
	
	

}
