package jwd.wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.wafepa.model.Zaposleni;
import jwd.wafepa.web.dto.ZaposleniDTO;

@Component
public class ZaposleniToZaposleniDTO 
                            implements Converter<Zaposleni, ZaposleniDTO>{

	@Override
	public ZaposleniDTO convert(Zaposleni zaposleni) {
		ZaposleniDTO dto = new ZaposleniDTO();
		dto.setId(zaposleni.getId());
		dto.setIme(zaposleni.getIme());
		dto.setPrezime(zaposleni.getPrezime());
		dto.setPozicija(zaposleni.getPozicija());
		dto.setBrojZadataka(zaposleni.getBrojZadataka());
		return dto;
	}
	
	public List<ZaposleniDTO> convert(List<Zaposleni> radnici){
		List<ZaposleniDTO> ret = new ArrayList<>();
		
		for(Zaposleni z: radnici){
			ret.add(convert(z));
		}
		return ret;
	}
	
	

}
