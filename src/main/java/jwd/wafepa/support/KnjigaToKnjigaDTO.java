package jwd.wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.wafepa.model.Knjiga;
import jwd.wafepa.web.dto.KnjigaDTO;

@Component
public class KnjigaToKnjigaDTO 
           implements Converter<Knjiga, KnjigaDTO>{

	@Override
	public KnjigaDTO convert(Knjiga knjiga) {
		KnjigaDTO dto = new KnjigaDTO();
		dto.setId(knjiga.getId());
		dto.setCena(knjiga.getCena());
		dto.setISBN(knjiga.getISBN());
		dto.setKolicina(knjiga.getKolicina());
		dto.setNaziv(knjiga.getNaziv());
		dto.setPisac(knjiga.getPisac());
		dto.setIzdavacId(knjiga.getIzdavac().getId());
		dto.setIzdavacNaziv(knjiga.getIzdavac().getNaziv());
		return dto;
	}
	
	public List<KnjigaDTO> convert(List<Knjiga> knjige){
		List<KnjigaDTO> ret = new ArrayList<>();
		
		for(Knjiga k:knjige){
			ret.add(convert(k));
		}
		return ret;
	}
	
	

}
