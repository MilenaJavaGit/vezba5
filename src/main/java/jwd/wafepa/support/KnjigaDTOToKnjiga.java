package jwd.wafepa.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.wafepa.model.Knjiga;
import jwd.wafepa.service.IzdavacService;
import jwd.wafepa.service.KnjigaService;
import jwd.wafepa.web.dto.KnjigaDTO;

@Component
public class KnjigaDTOToKnjiga 
                 implements Converter<KnjigaDTO, Knjiga>{
	@Autowired
	private KnjigaService knjigaService;
	@Autowired
	private IzdavacService izdavacService;

	@Override
	public Knjiga convert(KnjigaDTO dto) {
	    Knjiga knjiga;
	    if(dto.getId()==null){
	    	knjiga = new Knjiga();
	    	knjiga.setIzdavac(izdavacService.findOne(
	    			dto.getIzdavacId()));
	    }else{
	    	knjiga = knjigaService.findOne(dto.getId());
	    }
	    knjiga.setCena(dto.getCena());
	    knjiga.setISBN(dto.getISBN());
	    knjiga.setKolicina(dto.getKolicina());
	    knjiga.setNaziv(dto.getNaziv());
	    knjiga.setPisac(dto.getPisac());
	    	
		return knjiga;
	}
	
	

}
