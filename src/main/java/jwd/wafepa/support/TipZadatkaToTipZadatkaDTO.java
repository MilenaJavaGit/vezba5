package jwd.wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.wafepa.model.TipZadatka;
import jwd.wafepa.web.dto.TipZadatkaDTO;

@Component
public class TipZadatkaToTipZadatkaDTO 
              implements Converter<TipZadatka, TipZadatkaDTO>{

	@Override
	public TipZadatkaDTO convert(TipZadatka tip) {
		TipZadatkaDTO dto = new TipZadatkaDTO();
		dto.setId(tip.getId());
		dto.setNaziv(tip.getNaziv());
		return dto;
	}
	
	public List<TipZadatkaDTO> convert (List<TipZadatka> tipovi){
		List<TipZadatkaDTO> ret = new ArrayList<>();
		
		for(TipZadatka t: tipovi){
			ret.add(convert(t));
		}
		return ret;
	}

}
