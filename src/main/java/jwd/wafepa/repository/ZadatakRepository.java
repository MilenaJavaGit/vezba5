package jwd.wafepa.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import jwd.wafepa.model.Zadatak;

@Repository
public interface ZadatakRepository 
                    extends JpaRepository<Zadatak, Long>{
	
	Page<Zadatak> findByZaposleniId(Long zaposleniId, Pageable pageRequest);
    
	@Query("SELECT z FROM Zadatak z WHERE "
			+ "(:naziv IS NULL or z.naziv like :naziv ) AND "
			+ "(:minP IS NULL OR z.prioritet >= :minP ) AND "
			+ "(:zaposleniId IS NULL OR z.zaposleni.id like :zaposleniId ) AND "
			+ "(:maxP IS NULL OR z.prioritet <= :maxP)"
			)
	Page<Zadatak> pretraga(
			@Param("naziv") String naziv, 
			@Param("minP") Integer min, 
			@Param("maxP") Integer max,
			@Param("zaposleniId") Long zaposleniId,
			Pageable pageRequest);
}
