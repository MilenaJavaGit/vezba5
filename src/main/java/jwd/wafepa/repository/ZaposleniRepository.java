package jwd.wafepa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.wafepa.model.Zaposleni;

@Repository
public interface ZaposleniRepository 
              extends JpaRepository<Zaposleni, Long>{

}
