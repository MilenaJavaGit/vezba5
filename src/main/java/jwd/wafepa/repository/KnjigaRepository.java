package jwd.wafepa.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.wafepa.model.Knjiga;


@Repository
public interface KnjigaRepository 
                extends JpaRepository<Knjiga, Long>{
	
	Page<Knjiga> findByIzdavacId(Long izdavacId, Pageable pageRequest);
    
	@Query("SELECT k FROM Knjiga k WHERE "
			+ "(:naziv IS NULL or k.naziv like :naziv ) AND "
			+ "(:pisac IS NULL OR k.pisac like :pisac ) AND "
			
			+ "(:maxK IS NULL OR k.kolicina <= :maxK)"
			)
	Page<Knjiga> pretraga(
			@Param("naziv") String naziv, 
			@Param("pisac") String pisac, 
			@Param("maxK") Integer max,
		
			Pageable pageRequest);
}
