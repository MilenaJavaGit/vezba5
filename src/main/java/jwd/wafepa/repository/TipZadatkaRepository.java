package jwd.wafepa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.wafepa.model.TipZadatka;

@Repository
public interface TipZadatkaRepository 
                 extends JpaRepository<TipZadatka, Long>{

}
