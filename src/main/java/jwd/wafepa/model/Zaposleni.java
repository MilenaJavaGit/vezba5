package jwd.wafepa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table
public class Zaposleni {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String ime;
	@Column
	private String prezime;
	@Column
	private String pozicija;
	@Column
	private Integer brojZadataka;
	@OneToMany(mappedBy = "zaposleni" ,fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<Zadatak> zadaci = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getPozicija() {
		return pozicija;
	}
	public void setPozicija(String pozicija) {
		this.pozicija = pozicija;
	}
	public Integer getBrojZadataka() {
		return brojZadataka;
	}
	public void setBrojZadataka(Integer brojZadataka) {
		this.brojZadataka = brojZadataka;
	}
	public List<Zadatak> getZadaci() {
		return zadaci;
	}
	public void setZadaci(List<Zadatak> zadaci) {
		this.zadaci = zadaci;
	}
	
	public void addZadatak(Zadatak zadatak){
		this.zadaci.add(zadatak);
		
		if(!this.equals(zadatak.getZaposleni())){
			zadatak.setZaposleni(this);
		}
	}
	
	

}
