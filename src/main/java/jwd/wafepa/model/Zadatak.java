package jwd.wafepa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Zadatak {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String naziv;
	@Column
	private String opis;
	@Column
	private Integer prioritet;
	@Column
	private String rok;
	@ManyToOne(fetch=FetchType.EAGER)
	private Zaposleni zaposleni;
	@ManyToOne(fetch=FetchType.EAGER)
	private TipZadatka tipZadatka;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public Integer getPrioritet() {
		return prioritet;
	}
	public void setPrioritet(Integer prioritet) {
		this.prioritet = prioritet;
	}
	public String getRok() {
		return rok;
	}
	public void setRok(String rok) {
		this.rok = rok;
	}
	public Zaposleni getZaposleni() {
		return zaposleni;
	}
	public void setZaposleni(Zaposleni zaposleni) {
		this.zaposleni = zaposleni;
		
		if(zaposleni!= null && !zaposleni.getZadaci().contains(this)){
			zaposleni.getZadaci().add(this);
		}
	}
	public TipZadatka getTipZadatka() {
		return tipZadatka;
	}
	public void setTipZadatka(TipZadatka tipZadatka) {
		this.tipZadatka = tipZadatka;
		
		if(tipZadatka!=null && !tipZadatka.getZadaci().contains(this)){
			tipZadatka.getZadaci().add(this);
		}
	}
	
	

}
