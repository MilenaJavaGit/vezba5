package jwd.wafepa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table
public class TipZadatka {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String naziv;
	@OneToMany(mappedBy = "tipZadatka" ,fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<Zadatak> zadaci = new ArrayList<>();
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public List<Zadatak> getZadaci() {
		return zadaci;
	}
	public void setZadaci(List<Zadatak> zadaci) {
		this.zadaci = zadaci;
	}
	
	public void addZadatakTip(Zadatak zadatak){
		this.zadaci.add(zadatak);
		
		if(!this.equals(zadatak.getTipZadatka())){
			zadatak.setTipZadatka(this);
		}
	}
	
	

}
