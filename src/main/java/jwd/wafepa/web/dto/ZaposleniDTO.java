package jwd.wafepa.web.dto;

public class ZaposleniDTO {
	private Long id;
	private String ime;
	private String prezime;
	private String pozicija;
	private Integer brojZadataka;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getPozicija() {
		return pozicija;
	}
	public void setPozicija(String pozicija) {
		this.pozicija = pozicija;
	}
	public Integer getBrojZadataka() {
		return brojZadataka;
	}
	public void setBrojZadataka(Integer brojZadataka) {
		this.brojZadataka = brojZadataka;
	}
	
	
	

}
