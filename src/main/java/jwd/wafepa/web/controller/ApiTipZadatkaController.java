package jwd.wafepa.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.wafepa.service.TipZadatkaService;
import jwd.wafepa.support.TipZadatkaToTipZadatkaDTO;
import jwd.wafepa.web.dto.TipZadatkaDTO;


@RestController
@RequestMapping(value="/api/tipovi")
public class ApiTipZadatkaController {
	@Autowired
	private TipZadatkaService tipZadatkaService;
	@Autowired
	private TipZadatkaToTipZadatkaDTO toDTO;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<TipZadatkaDTO>> get(){
		return new ResponseEntity<>(
				toDTO.convert(tipZadatkaService.findAll()),
				HttpStatus.OK);
	}

}
