package jwd.wafepa.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.wafepa.model.Zadatak;
import jwd.wafepa.model.Zaposleni;
import jwd.wafepa.service.ZadatakService;
import jwd.wafepa.service.ZaposleniService;
import jwd.wafepa.support.ZadatakToZadatakDTO;
import jwd.wafepa.support.ZaposleniToZaposleniDTO;
import jwd.wafepa.web.dto.ZadatakDTO;
import jwd.wafepa.web.dto.ZaposleniDTO;


@RestController
@RequestMapping(value="/api/zaposleni")
public class ApiZaposleniController {
	@Autowired
	private ZaposleniService zaposleniService;
	@Autowired
	private ZadatakService zadatakService;
	@Autowired
	private ZaposleniToZaposleniDTO toDTO;
	@Autowired
	private ZadatakToZadatakDTO toZadatakDTO;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ZaposleniDTO>> get(){
		return new ResponseEntity<>(
				toDTO.convert(zaposleniService.findAll()),
				HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<ZaposleniDTO> get(
			@PathVariable Long id){
		
		Zaposleni zaposleni = zaposleniService.findOne(id);
		
		if(zaposleni == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(
				toDTO.convert(zaposleni),
				HttpStatus.OK);
	}
	
	@RequestMapping(value="/{zaposleniId}/zadaci")
	public ResponseEntity<List<ZadatakDTO>> zadaciZaposlenog(
			@PathVariable Long zaposleniId,
			@RequestParam(defaultValue="0") int pageNum){
		Page<Zadatak> zadaci = zadatakService.findByZaposleniId(pageNum, zaposleniId);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(zadaci.getTotalPages()) );
		return  new ResponseEntity<>(
				toZadatakDTO.convert(zadaci.getContent()),
				headers,
				HttpStatus.OK);
	}

}
