package jwd.wafepa.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.wafepa.model.Knjiga;
import jwd.wafepa.model.Kupovina;
import jwd.wafepa.service.KnjigaService;
import jwd.wafepa.service.KupovinaService;
import jwd.wafepa.support.KnjigaDTOToKnjiga;
import jwd.wafepa.support.KnjigaToKnjigaDTO;
import jwd.wafepa.support.KupovinaToKupovinaDTO;
import jwd.wafepa.web.dto.KnjigaDTO;
import jwd.wafepa.web.dto.KupovinaDTO;


@RestController
@RequestMapping("/api/knjige")
public class ApiKnjigaController {
	@Autowired
	private KnjigaService knjigaService;
	@Autowired
	private KupovinaService kupovinaService;
	@Autowired
	private KnjigaToKnjigaDTO toDTO;
	@Autowired
	private KnjigaDTOToKnjiga toKnjiga;
	@Autowired
	private KupovinaToKupovinaDTO toKupovinaDTO;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<KnjigaDTO>> get(
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String pisac,
			@RequestParam(required=false) Integer maxK,
			@RequestParam(defaultValue="0") int pageNum){
		
		Page<Knjiga> knjige;
		
		if(naziv != null || pisac != null || maxK != null) {
			knjige = knjigaService.pretraga(naziv, pisac, maxK, pageNum);
		}else{
			knjige = knjigaService.findAll(pageNum);
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(knjige.getTotalPages()) );
		return  new ResponseEntity<>(
				toDTO.convert(knjige.getContent()),
				headers,
				HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET,
					value="/{id}")
	public ResponseEntity<KnjigaDTO> get(
			@PathVariable Long id){
		Knjiga knjiga = knjigaService.findOne(id);
		
		if(knjiga==null){
			return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(
				toDTO.convert(knjiga),
				HttpStatus.OK);	
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<KnjigaDTO> add(
			@RequestBody KnjigaDTO novaKnjiga){
		
		Knjiga knjiga = toKnjiga.convert(novaKnjiga); 
		knjigaService.save(knjiga);
		
		return new ResponseEntity<>(toDTO.convert(knjiga),
				HttpStatus.CREATED);
	}
	@RequestMapping(method=RequestMethod.POST, value="/{id}/kupovina")
	public ResponseEntity<KupovinaDTO> buy(
			@PathVariable Long id){
		
		Kupovina kupovina = kupovinaService.buyABook(id);
		
		
		return new ResponseEntity<>(toKupovinaDTO.convert(kupovina),
				HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.PUT,
			value="/{id}")
	public ResponseEntity<KnjigaDTO> edit(
			@PathVariable Long id,
			@RequestBody KnjigaDTO izmenjena){
		
		if(!id.equals(izmenjena.getId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Knjiga knjiga = toKnjiga.convert(izmenjena); 
		knjigaService.save(knjiga);
		
		return new ResponseEntity<>(toDTO.convert(knjiga),
				HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,
			value="/{id}")
	public ResponseEntity<KnjigaDTO> delete(@PathVariable Long id){
		knjigaService.remove(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
