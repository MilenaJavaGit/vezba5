package jwd.wafepa.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.wafepa.model.Zadatak;
import jwd.wafepa.service.ZadatakService;
import jwd.wafepa.support.ZadatakDTOToZadatak;
import jwd.wafepa.support.ZadatakToZadatakDTO;

import jwd.wafepa.web.dto.ZadatakDTO;

@RestController
@RequestMapping(value="/api/zadaci")
public class ApiZadatakController {
	@Autowired
	private ZadatakService zadatakService;
	@Autowired
	private ZadatakToZadatakDTO toDTO;
	@Autowired
	private ZadatakDTOToZadatak toZadatak;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ZadatakDTO>> get(
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) Integer minP,
			@RequestParam(required=false) Integer maxP,
			@RequestParam(required=false) Long zaposleniId,
			@RequestParam(defaultValue="0") int pageNum){
		
		Page<Zadatak> zadaci;
		
		if(naziv != null || minP != null || maxP != null || zaposleniId!=null) {
			zadaci = zadatakService.pretraga(naziv, minP, maxP, zaposleniId, pageNum);
		}else{
			zadaci = zadatakService.findAll(pageNum);
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(zadaci.getTotalPages()) );
		return  new ResponseEntity<>(
				toDTO.convert(zadaci.getContent()),
				headers,
				HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET,
					value="/{id}")
	public ResponseEntity<ZadatakDTO> get(
			@PathVariable Long id){
		Zadatak zadatak = zadatakService.findOne(id);
		
		if(zadatak==null){
			return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(
				toDTO.convert(zadatak),
				HttpStatus.OK);	
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<ZadatakDTO> add(
			@RequestBody ZadatakDTO noviZadatak){
		
		Zadatak zadatak = toZadatak.convert(noviZadatak); 
		zadatakService.save(zadatak);
		
		return new ResponseEntity<>(toDTO.convert(zadatak),
				HttpStatus.CREATED);
	}

	
	@RequestMapping(method=RequestMethod.PUT,
			value="/{id}")
	public ResponseEntity<ZadatakDTO> edit(
			@PathVariable Long id,
			@RequestBody ZadatakDTO izmenjen){
		
		if(!id.equals(izmenjen.getId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Zadatak zadatak = toZadatak.convert(izmenjen); 
		zadatakService.save(zadatak);
		
		return new ResponseEntity<>(toDTO.convert(zadatak),
				HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,
			value="/{id}")
	public ResponseEntity<ZadatakDTO> delete(@PathVariable Long id){
		zadatakService.remove(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
